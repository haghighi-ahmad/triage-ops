# frozen_string_literal: true

require 'yaml'

module GroupDefinition
  module_function

  DATA = YAML.load_file(File.expand_path("#{__dir__}/../group-definition.yml")).freeze

  DEFAULT_ASSIGNEE_FIELDS = %w[
    pm
    engineering_manager
    backend_engineering_manager
    frontend_engineering_manager
    extra_assignees
  ].freeze

  DATA.each do |name, definition|
    define_method("group_#{name}") do |fields = nil|
      assignee_fields =
        if fields
          fields & DEFAULT_ASSIGNEE_FIELDS
        else
          DEFAULT_ASSIGNEE_FIELDS
        end

      result = {
        assignees:
          definition.values_at(*assignee_fields).flatten.compact.uniq,
        labels:
          definition['labels'] || ["group::#{name.tr('_', ' ')}"]
      }

      if fields.nil? || fields.include?('extra_mentions')
        if extra_mentions = definition['extra_mentions']
          result[:mentions] = (result[:assignees] + extra_mentions).uniq
        end
      end

      result
    end
  end
end
