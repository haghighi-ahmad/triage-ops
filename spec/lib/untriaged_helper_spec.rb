# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/untriaged_helper'

RSpec.describe UntriagedHelper do
  let(:resource_klass) do
    Struct.new(:labels) do
      include UntriagedHelper
    end
  end

  let(:label_klass) do
    Struct.new(:name)
  end

  let(:labels) { [] }

  let(:team_from_www) do
    {
      'user1' => { 'departments' => ['Quality Department', 'Merge Request coach'] },
      'user2' => { 'departments' => ['Quality Department', 'Merge Request coach'] },
      'user3' => { 'departments' => ['Quality Department'] },
      'user4' => { 'role' => 'Director of Quality', 'departments' => ['Quality Department'] }
    }
  end

  let(:roulette) do
    [
      { 'username' => 'user1' },
      { 'username' => 'user2', 'out_of_office' => true },
      { 'username' => 'user3', 'out_of_office' => false },
      { 'username' => 'user4', 'out_of_office' => false }
    ]
  end

  subject { resource_klass.new(labels) }

  describe '#unlabelled_issues_triagers' do
    it 'retrieves merge request coaches from www-gitlab-com' do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)
      allow(WwwGitLabCom).to receive(:roulette).and_return(roulette)

      expect(subject.unlabelled_issues_triagers).to match_array(%w[@user1 @user3])
    end
  end

  describe '#merge_request_coaches' do
    it 'retrieves merge request coaches from www-gitlab-com' do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)
      allow(WwwGitLabCom).to receive(:roulette).and_return(roulette)

      expect(subject.merge_request_coaches).to match_array(%w[@user1])
    end
  end

  describe '#distribute_items' do
    let(:list_items) { (1..7).to_a.map { |i| "Item ##{i}"} }
    let(:potential_triagers) { %w[@triager-3 @triager-2 @triager-1] }

    before do
      allow(subject).to receive(:puts)
    end

    it 'distributes items all items across triagers' do
      distribution = subject.distribute_items(list_items, potential_triagers)
      items = distribution.values.inject(&:+)

      expect(items).to match_array(list_items)
    end

    it 'sorts triagers by username' do
      distribution = subject.distribute_items(list_items, potential_triagers)
      triagers = distribution.keys

      expect(triagers).to eq(potential_triagers.sort)
    end
  end

  describe '#untriaged?' do
    context 'for triaged resource' do
      let(:labels) { [label_klass.new('group::runner'), label_klass.new('devops::verify'), label_klass.new('bug')] }

      it 'returns false' do
        expect(subject.untriaged?).to eq(false)
      end
    end

    context 'for untriaged resource' do
      context 'no stage label' do
        let(:labels) { [label_klass.new('group::runner'), label_klass.new('bug')] }

        it 'returns true' do
          expect(subject.untriaged?).to eq(true)
        end
      end

      context 'no group label' do
        let(:labels) { [label_klass.new('devops::verify'), label_klass.new('bug')] }

        it 'returns true' do
          expect(subject.untriaged?).to eq(true)
        end
      end

      context 'no type label' do
        let(:labels) { [label_klass.new('group::runner'), label_klass.new('devops::verify')] }

        it 'returns true' do
          expect(subject.untriaged?).to eq(true)
        end
      end
    end

    context 'for unlabelled resource' do
      it 'returns true' do
        expect(subject.untriaged?).to eq(true)
      end
    end
  end
end
